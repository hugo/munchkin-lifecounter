/* Bit of a hack to force HTTPS. Not strictly neccesary, but
 * localStorage isn't shared between http and https access, so this
 * will simply remove a source of confusion */
if (window.location.protocol !== 'https:') {
	let location = `${window.location}`
    window.location = `https:${location.substring(window.location.protocol.length)}`
}

async function loadInfo(gameid, element) {
	let players = JSON.parse(window.localStorage.getItem(gameid));
	let ul = document.createElement('ul');
	for (let name in players) {
		let li = document.createElement('li');
		li.textContent = `${name}: ${players[name]}`
		ul.appendChild(li);
	}
	element.appendChild(ul);
}

let confirmPromise = null;

function closePopup(event, status) {
	confirmPromise(status);
}

async function confirmPopup (message) {
	let el = document.getElementById('confirm-popup');
	el.querySelector('p').textContent = message;
	el.removeAttribute('data-invisible');

	let p = new Promise((resolve, reject) => {
		confirmPromise = resolve;
	})

	let result = await p;

	el.setAttribute('data-invisible', 'true');

	return result;
}

window.onload = function () {
	let games = JSON.parse(window.localStorage.getItem('games')) || []
	let el = document.querySelector(".stored-games");
	for (let game of games) {
		let child = document.createElement('li');

		let anchor = document.createElement('a');
		anchor.href = `munchkin.html?gameid=${game}`
		anchor.textContent = game;

		let button = document.createElement('button');
		button.textContent = 'Ta bort';
		button.onclick = async function () {
			let result = await confirmPopup(`Verkligen ta bort ${game}`)
			if (result) {
				window.localStorage.removeItem(game);
				let games = JSON.parse(window.localStorage.getItem('games'));
				games = games.filter(g => g != game)
				window.localStorage.setItem('games', JSON.stringify(games));
				child.remove();
			}
		}

		child.replaceChildren(anchor, button)

		el.appendChild(child)

		loadInfo(game, child);
	}

	document.getElementById('delete-everything').onclick = async function () {
		let games = JSON.parse(window.localStorage.getItem('games')) || []
		let n = games.length;
		let result = await confirmPopup(`Ta verkligen bort alla ${n} partier?`)
		if (result) {
			for (let game of games) {
				window.localStorage.removeItem(game);
			}
			window.localStorage.setItem('games', "[]")
			let el = document.querySelector(".stored-games");
			let child;
			while ((child = el.firstElementChild)) {
				child.remove();
			}
		}
	}

	let form = document.querySelector("form#new-game")

	form.addEventListener('submit', function (event) {
		let playerNames = []
		let players = {}
		gameid = (new Date).toISOString();
		for (var i = 1; i < 5; i++) {
			const pname = document.getElementsByName("p" + i)[0].value
			if (pname != null && pname != "") {
				playerNames.push(pname);
				players[pname] = 0;
			}
		}
		window.localStorage.setItem(gameid, JSON.stringify(players))

		let games = JSON.parse(window.localStorage.getItem('games')) || []
		games.push(gameid);
		window.localStorage.setItem('games', JSON.stringify(games))

		event.preventDefault();

		window.location = `munchkin.html?gameid=${gameid}`
	})
}

/* // generate lots of dummy test data
function generateData() {
	let games = JSON.parse(window.localStorage.getItem('games')) || []
	for (let i = 0; i < 100; i++) {
		let gameid = Math.random();
		let data = {}
		for (let i = 0; i < 4; i++) {
			data[Math.random()] = Math.floor(Math.random() * 10)
		}
		games.push(gameid)
		window.localStorage.setItem(gameid, JSON.stringify(data))
	}
	window.localStorage.setItem('games', JSON.stringify(games));
	window.location = window.location;
}
*/
