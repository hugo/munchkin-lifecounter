// use "strict";
function changeLife(el, change) {
	el.children[1].innerText -= -change;
}

function rolldice() {

 let faces =
  [[[50, 50]],

   [[25, 25],
    [75, 75]],

   [[25, 25],
    [50, 50],
    [75, 75]],

   [[25, 25],
    [25, 75],
    [75, 25],
    [75, 75]],

   [[25, 25],
    [25, 75],
    [50, 50],
    [75, 25],
    [75, 75]],

   [[25, 25],
    [25, 50],
    [25, 75],
    [75, 25],
    [75, 50],
    [75, 75]],
  ]



 let roll = Math.floor(Math.random() * 6);
 let x, y;
 let i = 0;
 let circles = document.querySelectorAll("circle");
 for ([x, y] of faces[roll]) {
  circles[i].setAttribute('cx', x);
  circles[i].setAttribute('cy', y);
  circles[i].setAttribute('visibility', 'visible');
  i++;
 }
 for (;i < 6; i++) {
  circles[i].setAttribute('visibility', 'hidden');
 }

}

function err(what) {
	let err = document.getElementById('error').content.firstElementChild.cloneNode(true);
	err.querySelector('.text').textContent = what
	document.querySelector('.root').replaceChildren(err);
}

window.onload = function() {

	const urlParams = new URLSearchParams(window.location.search);


	let gameid;
	if (! (gameid = urlParams.get('gameid'))) {
		err('URL-parametern gameid är obligatorisk')
		return;
	}

	let playerNames = [];
	let players = JSON.parse(window.localStorage.getItem(gameid));
	if (players == null) {
		err(`Ingen spel finns med id:t [${gameid}]`)
		return;
	}


	for (let name in players) {
		playerNames.push(name);
	}

	if (playerNames.length == 0) {
		err(`Ett spel måste ha minst 1 spelare`)
		return;
	}

	document.querySelector('#bookmark').href = `munchkin.html?gameid=${gameid}`

	const counterTemplate = document.getElementById("life-templ").content.firstElementChild;
	const countDiv = document.getElementsByClassName("counters")[0];
	playerNames.forEach(function(player) {
		const el = counterTemplate.cloneNode(true);
		const subel = el.querySelector(".lifecounter-sub");

		el.querySelector(".lifename").textContent = player;
		el.querySelector(".life-count").textContent = players[player];

		countDiv.appendChild(el);
		subel.querySelector(".btn-up").onclick = function () {
			players[player] += 1;
			el.querySelector(".life-count").textContent = players[player];
			window.localStorage.setItem(gameid, JSON.stringify(players))
		};
		subel.querySelector(".btn-down").onclick = function () {
			players[player] -= 1;
			el.querySelector(".life-count").textContent = players[player];
			window.localStorage.setItem(gameid, JSON.stringify(players))
		};
	});
};
